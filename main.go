package main

import (
	"os"
	"fmt"
	"encoding/json"
	"strings"

	"gitlab.com/scabala/zelligo"
)

type Plugin struct {
	tabs map[string]bool
	testRuns uint32
	config map[string]string
	modeLog map[string]int
}


func (p *Plugin) Load(configuration map[string]string) error {
	fmt.Fprintf(os.Stderr, "Load\n")

	p.modeLog = make(map[string]int)
	p.tabs = make(map[string]bool)
	p.config = configuration

	zelligo.RequestPermission([]zelligo.PermissionType{
		zelligo.PermissionType_ReadApplicationState, zelligo.PermissionType_RunCommands,
	})

	zelligo.Subscribe([]zelligo.EventType{
		zelligo.EventType_ModeUpdate,
		zelligo.EventType_TabUpdate,
		zelligo.EventType_Key,
	})

	return nil
}

func (p *Plugin) Update(event zelligo.Event) (bool, error) {
	var shouldRender bool

	switch event.Name{
	case zelligo.EventType_ModeUpdate:
		modeUpdatePayload := event.GetModeUpdatePayload()
		if modeUpdatePayload == nil {
			return false, nil
		}
		mode := modeUpdatePayload.GetCurrentMode().String()
		v, ok := p.modeLog[mode]
		if !ok {
			p.modeLog[mode] = 0
		} else {
			p.modeLog[mode] = v + 1
		}
	
	case zelligo.EventType_TabUpdate:
		tabUpdatePayload := event.GetTabUpdatePayload()
		if tabUpdatePayload == nil {
			return false, nil
		}
		for _, t := range tabUpdatePayload.GetTabInfo() {
			p.tabs[t.Name] = true
		}
		shouldRender = true
	case zelligo.EventType_Key:
		keyPayload := event.GetKeyPayload()
		if keyPayload == nil {
			return false, nil
		}
		if keyPayload.GetChar() == zelligo.Key_n {
			p.testRuns = p.testRuns + 1
			err := zelligo.OpenCommandPaneFloating(&zelligo.Command{
				Path: "go",
				Args: []string{"test"},
				Cwd: nil,
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error when launching tests: %v\n", err)	
			}
		}
	}

	return shouldRender, nil
}

func (p *Plugin) Render(cols uint32, rows uint32) error {
	fmt.Printf("\nI have %d rows and %d columns\n", rows, cols)
	jsonConfig, _ := json.Marshal(p.config)
	fmt.Printf("\nI was started with the following user configuration: %s\n", string(jsonConfig))

	fmt.Println("Modes:")
	for mode, count := range p.modeLog {
		fmt.Printf("%s -> Changed %d times\n", mode, count)
	}

	tabs := make([]string, 0, len(p.tabs))
	for t, _ := range p.tabs {
		tabs = append(tabs, t)
	}
	fmt.Printf("\nCurrent tabs: \n", strings.Join(tabs, ","))

	if p.testRuns >0 {
		fmt.Printf("\nTests run %d times!\n", p.testRuns)
	}

	return nil
}

func init() {
	zelligo.RegisterPlugin(&Plugin{})
}

func main() {}  // this is very important
