module example.com/zelligo-example

go 1.21

replace gitlab.com/scabala/zelligo => /mnt/zelligo

require gitlab.com/scabala/zelligo v0.0.0-20230828102631-f59af7a43705

require google.golang.org/protobuf v1.31.0 // indirect
